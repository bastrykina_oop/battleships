#include "my_set.h"

my_set::my_set() {
	for (int i = 0; i <= 9; i++) {
		indexes_size = 10;
		indexes[i] = i;
		v_sizes[i] = 10;
		for (int j = 0; j <= 9; j++) {
			v[i][j] = j;
		}
	}
}

bool my_set::find(int i, int j, int& real_i, int& real_j) {
	bool is_found = false;
	for (int k = 0; k <= indexes_size - 1; k++) {
		if (indexes[k] == i) {
			real_i = indexes[k];
			is_found = true;
			break;
		}
	}
	if (!is_found) return false;
	for (int k = 0; k <= v_sizes[real_i] - 1; k++) if (v[real_i][k] == j) {
		real_j = k;
		return true;
	}
	 return false;
}

void my_set::erase(int i, int j) {
	int real_i, real_j;
	if (!find(i, j, real_i, real_j)) return;
	v[real_i][real_j] = v[real_i][v_sizes[real_i] - 1];
	v_sizes[real_i]--;
	if (!v_sizes[real_i]) {
		for (int k = 0; k <= indexes_size - 1; k++) if (indexes[k] == real_i) {
			indexes[k] = indexes[indexes_size - 1];
			indexes_size--;
			break;
		}
	}
}

void my_set::get_random(int& i, int& j) {
	srand(time(0));
	i = indexes[rand() % indexes_size];
	j = v[i][rand() % v_sizes[i]];
}

void my_set::print() {
	for (int i = 0; i < indexes_size; i++) {
		std::cout << indexes[i] << " ";
		for (int j = 0; j < v_sizes[indexes[i]]; j++) std::cout << v[indexes[i]][j];
		std::cout << std::endl;
	}
}
