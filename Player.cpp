#include "Battleships.h"
#include "Player.h"

bool Player::check_surroundings(int a, int b, Direction d, int decks) {
	switch (d) {
	case Direction::Right: {
		for (int i = -1; i <= 1; i++)
			for (int j = -1; j <= decks; j++)
				if ((a + i >= 0) && (a + i <= 9) && (b + j >= 0) && (b + j <= 9) && (ships[a + i][b + j])) return false;
		return true;
	}
	case Direction::Down: {
		for (int i = -1; i <= decks; i++)
			for (int j = -1; j <= 1; j++)
				if ((a + i >= 0) && (a + i <= 9) && (b + j >= 0) && (b + j <= 9) && (ships[a + i][b + j])) return false;
		return true;
	}
	}
}

Player::Player() {
	cur_shoot.i = 0;
	cur_shoot.j = 0;
	cur_shoot.d = Direction::Left;
	cur_shoot.decks = 0;
	for (int i = 0; i <= 9; i++)
		for (int j = 0; j <= 9; j++) {
			shoots[i][j] = ShootStatus::Unknown;
			ships[i][j] = 0;
		}
}

void Player::arrange() {
	srand(time(0));
	//Placing 1-deck ships
	int k1 = 4, k2 = 3, k3 = 2, k4 = 1;
	while (k1) {
		int i = rand() % 10;
		int j = rand() % 10;
		if (check_surroundings(i, j, Direction::Down, 1)) {
			ships[i][j] = 1;
			k1--;
		}
	}
	//Placing 2-deck ships
	while (k2) {
		int i, j;
		Direction d = Direction(rand() % 2);
		switch (d) {
		case Direction::Right: {
			i = rand() % 10;
			j = rand() % 9;
			break;
		}
		case Direction::Down: {
			i = rand() % 9;
			j = rand() % 10;
			break;
		}
		}
		if (check_surroundings(i, j, d, 2)) {
			ships[i][j] = 1;
			if (d == Direction::Right) ships[i][j + 1] = 1;
			else ships[i + 1][j] = 1;
			k2--;
		}
	}
	//3-deck
	while (k3) {
		int i, j;
		Direction d = Direction(rand() % 2);
		switch (d) {
		case Direction::Right: {
			i = rand() % 10;
			j = rand() % 8;
			break;
		}
		case Direction::Down: {
			i = rand() % 8;
			j = rand() % 10;
			break;
		}
		}
		if (check_surroundings(i, j, d, 3)) {
			ships[i][j] = 1;
			if (d == Direction::Right) {
				ships[i][j + 1] = 1;
				ships[i][j + 2] = 1;
			}
			else {
				ships[i + 1][j] = 1;
				ships[i + 2][j] = 1;
			}
			k3--;
		}
	}
	//4-deck
	while (k4) {
		int i, j;
		Direction d = Direction(rand() % 2);
		switch (d) {
		case Direction::Right: {
			i = rand() % 10;
			j = rand() % 7;
			break;
		}
		case Direction::Down: {
			i = rand() % 7;
			j = rand() % 10;
			break;
		}
		}
		if (check_surroundings(i, j, d, 4)) {
			ships[i][j] = 1;
			if (d == Direction::Right) {
				ships[i][j + 1] = 1;
				ships[i][j + 2] = 1;
				ships[i][j + 3] = 1;
			}
			else {
				ships[i + 1][j] = 1;
				ships[i + 2][j] = 1;
				ships[i + 3][j] = 1;
			}
			k4--;
		}
	}
}

int* Player::get_arrangement() {
	return &ships[0][0];
}

void Player::shoot(int& x, int& y) {
	srand(time(0));
	if (shoots[cur_shoot.i][cur_shoot.j] == ShootStatus::Missed) {
		if (cur_shoot.decks > 1) {
			switch (cur_shoot.d) {
			case (Direction::Left): {
				cur_shoot.d = Direction::Right;
				x = cur_shoot.j += cur_shoot.decks + 1;
				y = cur_shoot.i;
				break;
			}
			case (Direction::Right): {
				cur_shoot.d = Direction::Left;
				x = cur_shoot.j -= cur_shoot.decks + 1;
				y = cur_shoot.i;
				break;
			}
			case (Direction::Up): {
				cur_shoot.d = Direction::Down;
				x = cur_shoot.j;
				y = cur_shoot.i += cur_shoot.decks + 1;
				break;
			}
			case (Direction::Down): {
				cur_shoot.d = Direction::Up;
				x = cur_shoot.j;
				y = cur_shoot.i -= cur_shoot.decks + 1;
				break;
			}
			}
			return;
		}
		else if (cur_shoot.decks == 1) {	
			switch (cur_shoot.d) {
			case Direction::Left: {
				x = (++cur_shoot.j);
				y = cur_shoot.i;
				break;
			};
			case Direction::Right: {
				x = (--cur_shoot.j);
				y = cur_shoot.i;
				break;
			}
			case Direction::Down: {
				y = (--cur_shoot.i);
				x = cur_shoot.j;
				break;
			}
			case Direction::Up: {
				y = (++cur_shoot.i);
				x = cur_shoot.j;
				break;
			}
			}
			while ((x < 0) || (y < 0) || (x > 9) || (y > 9) || (shoots[y][x] != ShootStatus::Unknown)) {
				cur_shoot.d = Direction(rand() % 4);
				switch (cur_shoot.d)
				{
				case Direction::Left: {
					x = cur_shoot.j - 1;
					y = cur_shoot.i;
					break;
				}
				case Direction::Right: {
					x = cur_shoot.j + 1;
					y = cur_shoot.i;
					break;
				}
				case Direction::Up: {
					x = cur_shoot.j;
					y = cur_shoot.i - 1;
					break;
				}
				case Direction::Down: {
					x = cur_shoot.j;
					y = cur_shoot.i + 1;
					break;
				}
				}
			}
			cur_shoot.j = x;
			cur_shoot.i = y;
			return;
		}
	}
	if (shoots[cur_shoot.i][cur_shoot.j] == ShootStatus::Hitted) {
		if (cur_shoot.decks > 1) {
			switch (cur_shoot.d) {
			case (Direction::Left): {
				if ((cur_shoot.j != 0) && (shoots[cur_shoot.i][cur_shoot.j - 1] == ShootStatus::Unknown)) {
					y = cur_shoot.i;
					x = (--cur_shoot.j);
				}
				else {
					cur_shoot.d = Direction::Right;
					y = cur_shoot.i;
					x = cur_shoot.j += cur_shoot.decks;
				}
				break;
			}
			case (Direction::Right): {
				if ((cur_shoot.j != 9) && (shoots[cur_shoot.i][cur_shoot.j + 1] == ShootStatus::Unknown)) {
					y = cur_shoot.i;
					x = (++cur_shoot.j);
				}
				else {
					cur_shoot.d = Direction::Left;
					y = cur_shoot.i;
					x = cur_shoot.j -= cur_shoot.decks;
				}
				break;
			}
			case (Direction::Up): {
				if ((cur_shoot.i != 0) && ((shoots[cur_shoot.i - 1][cur_shoot.j] == ShootStatus::Unknown))) {
					x = cur_shoot.j;
					y = (--cur_shoot.i);

				}
				else {
					cur_shoot.d = Direction::Down;
					x = cur_shoot.j;
					y = cur_shoot.i += cur_shoot.decks;
				}
				break;
			}
			case (Direction::Down): {
				if ((cur_shoot.j != 9) && ((shoots[cur_shoot.i + 1][cur_shoot.j] == ShootStatus::Unknown))) {
					x = cur_shoot.j;
					y = (++cur_shoot.i);
				}
				else {
					cur_shoot.d = Direction::Up;
					x = cur_shoot.j;
					y = cur_shoot.i -= cur_shoot.decks;
				}
				break;
			}
			}
		}
		else {
			x = cur_shoot.j;
			y = cur_shoot.i;
			while ((x < 0) || (y < 0) || (x > 9) || (y > 9) || (shoots[y][x] != ShootStatus::Unknown)) {
				cur_shoot.d = Direction(rand() % 4);
				switch (cur_shoot.d)
				{
				case Direction::Left: {
					x = cur_shoot.j - 1;
					y = cur_shoot.i;
					break;
				}
				case Direction::Right: {
					x = cur_shoot.j + 1;
					y = cur_shoot.i;
					break;
				}
				case Direction::Up: {
					x = cur_shoot.j;
					y = cur_shoot.i - 1;
					break;
				}
				case Direction::Down: {
					x = cur_shoot.j;
					y = cur_shoot.i + 1;
					break;
				}
				}
			}
			cur_shoot.j = x;
			cur_shoot.i = y;
		}
		return;
	}
	remained_shoots.get_random(cur_shoot.i, cur_shoot.j);
	y = cur_shoot.i;
	x = cur_shoot.j;
	return;
}

void Player::change_enemy_status(std::string msg) {
	if (msg == "Miss") {
		remained_shoots.erase(cur_shoot.i, cur_shoot.j);
		shoots[cur_shoot.i][cur_shoot.j] = ShootStatus::Missed;
	    return;
	}
	if (msg == "Hit") {
		cur_shoot.decks++;
		shoots[cur_shoot.i][cur_shoot.j] = ShootStatus::Hitted;
		return;
	}
	if (msg == "Kill") {
		int cur_shoot_i = cur_shoot.i, cur_shoot_j = cur_shoot.j;
		if (cur_shoot.d == Direction::Right || cur_shoot.d == Direction::Left) {
			if (cur_shoot.d == Direction::Right) cur_shoot_j -= cur_shoot.decks;
			for (int i = -1; i <= 1; i++)
				for (int j = -1; j <= cur_shoot.decks + 1; j++) {
					if ((cur_shoot_i + i >= 0) && (cur_shoot_i + i <= 9) && (cur_shoot_j + j >= 0) && (cur_shoot_j + j <= 9)) {
						remained_shoots.erase(cur_shoot_i + i, cur_shoot_j + j);
						shoots[cur_shoot_i + i][cur_shoot_j + j] = ShootStatus::Dead;
					}
				}
		}
		if (cur_shoot.d == Direction::Down || cur_shoot.d == Direction::Up) {
			if (cur_shoot.d == Direction::Down) cur_shoot_i -= cur_shoot.decks;
			for (int i = -1; i <= cur_shoot.decks + 1; i++)
				for (int j = -1; j <= 1; j++) {
					if ((cur_shoot_i + i >= 0) && (cur_shoot_i + i <= 9) && (cur_shoot_j + j >= 0) && (cur_shoot_j + j <= 9)) {
						remained_shoots.erase(cur_shoot_i + i, cur_shoot_j + j);
						shoots[cur_shoot_i + i][cur_shoot_j + j] = ShootStatus::Dead;
					}
				}
		}
		cur_shoot.d = Direction::Left;
		cur_shoot.decks = 0;
		shoots[cur_shoot.i][cur_shoot.j] = ShootStatus::Killed;
	}
}