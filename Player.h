#pragma once
#include "my_set.h"

enum class ShootStatus { Unknown, Missed, Hitted, Killed, Dead };
enum class Direction { Right, Down, Left, Up };

class Player {
private:
	int ships[10][10];
	ShootStatus shoots[10][10];
	struct Shoot {
		int i, j, decks;
		Direction d;
	} cur_shoot;
	my_set remained_shoots;
	bool check_surroundings(int i, int j, Direction d, int decks);
public:
	Player(); 
	void arrange();
	int*  get_arrangement();
	void shoot(int& x, int& y);
	void change_enemy_status(std::string msg);
};                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           