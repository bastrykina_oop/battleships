#pragma once
#include "Battleships.h"

class my_set {
	int v[10][10];
	int v_sizes[10];
	int indexes[10];
	int indexes_size;
public:
	my_set();
	bool find(int i, int j, int& real_i, int& real_j);
	void get_random(int& i, int& j);
	void erase(int i, int j);
	void print();
};