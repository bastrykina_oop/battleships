#include "Battleships.h"
#include "Game.h"
#include "Player.h"


Game::Game() : player() {}

int Game::start() {	
	char msg[255] = {"\0"};
	int x = 0, y = 0;
	while (std::cin >> msg) { 
		if (std::string(msg) == "Arrange!") {
			player.arrange();
			int* a = player.get_arrangement();
			for (int i = 0; i <= 9; i++) {
				for (int j = 0; j <= 9; j++) std::cout << a[i * 10 + j];
				std::cout << std::endl;
			}
			continue;
		}
		if (std::string(msg) == "Shoot!") {
			player.shoot(x, y);
			std::cout << char(int('A') + x) << " " << y << std::endl;
			continue;
		}
		if (std::string(msg) == "Hit" || std::string(msg) == "Kill") {
			player.change_enemy_status(msg);
			player.shoot(x, y);
			std::cout << char(int('A') + x) << " " << y << std::endl;
			continue;
		}
	    if (std::string(msg) == "Miss") player.change_enemy_status(msg);
	    if (std::string(msg) == "Win!" || std::string(msg) == "Lose") return 0;
	}
	return -1;
}

